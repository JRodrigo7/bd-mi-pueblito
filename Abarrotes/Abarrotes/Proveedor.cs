﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.Abarrotes;
using Logicanegocios.Abarrotes;

namespace Abarrotes
{
    public partial class Proveedor : Form
    {
        private ProveedorManejador _proveedormanejador;
        public Proveedor()
        {
            InitializeComponent();
            _proveedormanejador = new ProveedorManejador();
        }

        private void Proveedor_Load(object sender, EventArgs e)
        {
            lblNumero.Visible = false;
            BuscarProveedor("");
            LimpiarCuadros();
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
        }
        private void BuscarProveedor(string filtro)
        {
            dgv1.DataSource = _proveedormanejador.GetProveedor(filtro);
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarProveedor();
                LimpiarCuadros();
                BuscarProveedor("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void GuardarProveedor()
        {
            _proveedormanejador.Guardar(new Entidades.Abarrotes.Proveedor
            {
                Idproveedor = Convert.ToInt32(lblNumero.Text),
                Nombre = txtNombre.Text,
                Direccion = txtDireccion.Text,
                Telefono = Convert.ToInt32(txtTelefono.Text)
            });
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminarproveedor();
                    BuscarProveedor("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void Eliminarproveedor()
        {
            var idproveedor = dgv1.CurrentRow.Cells["idproveedor"].Value;
            _proveedormanejador.Eliminar(Convert.ToInt32(idproveedor));
        }

        private void ModificarProveedor()
        {
            

            
            lblNumero.Visible = true;

            lblNumero.Text = dgv1.CurrentRow.Cells["Idproveedor"].Value.ToString();
            txtNombre.Text = dgv1.CurrentRow.Cells["Nombre"].Value.ToString();
            txtDireccion.Text = dgv1.CurrentRow.Cells["Direccion"].Value.ToString();
            txtTelefono.Text = dgv1.CurrentRow.Cells["Telefono"].Value.ToString();
            
        }

        private void Dgv1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarProveedor();
                BuscarProveedor("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
