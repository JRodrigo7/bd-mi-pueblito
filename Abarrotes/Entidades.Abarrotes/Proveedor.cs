﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Abarrotes
{
    public class Proveedor
    {
        private int _idproveedor;
        private string _nombre;
        private string _direccion;
        private int _telefono;

        public int Idproveedor { get => _idproveedor; set => _idproveedor = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public int Telefono { get => _telefono; set => _telefono = value; }
    }
}
